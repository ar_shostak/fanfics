import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import {connect} from 'react-redux';
import { withAlert } from 'react-alert'
import Preloader from '../preloader/Preloader';
import { switchIsFetching } from './../store/UsersReducer';
import { setArticle, setChapters, setComments, checkChapter } from './../store/ArticleOverviewReducer';
import ArticleOverview from './ArticleOverview';
import ChapterService from './../service/ChapterService';
import ArticlesService from './../service/ArticlesService';
import LikesService from './../service/LikesService';
import CommentsService from './../service/CommentsService';

const CREATE = 'CREATE';
const DELETE = 'DELETE';
const EDIT = 'EDIT';
class ArticleOverviewContainer extends Component {
    constructor(props){
        super(props);
        this.componentDidMount = this.componentDidMount.bind(this);
        this.isOwner = this.isOwner.bind(this);
        this.getContent = this.getContent.bind(this);
        this.getArticle = this.getArticle.bind(this);
        this.postComment = this.postComment.bind(this);
        this.postLike = this.postLike.bind(this);
        this.postRate = this.postRate.bind(this);
        this.doAction = this.doAction.bind(this);
    }

    isOwner = () =>{
        if(this.props.article.userId === this.props.user.id) {
            return true;
        }
        return false;
    }

    componentDidMount(){
        this.props.switchIsFetching({isFetching : true});
        this.getArticle();
        this.getContent();
        this.props.switchIsFetching({isFetching : false});
    }

    componentWillUnmount(){
        this.props.checkChapter({checked : {}});
    }

    getArticle(){
        ArticlesService.getArticle({articleId : this.props.match.params.id, alert : this.props.alert,
            user : this.props.user}).then(data => {this.props.setArticle({article : data});})
    }

    getContent(){
        let params = {articleId : this.props.match.params.id, alert : this.props.alert, user : this.props.user};    
            ArticlesService.getChapters(params).then(data => {this.props.setChapters({chapters : data})});
            CommentsService.getComments(params).then(data => {this.props.setComments({comments : data})}); 
    }

    postComment(commentData){
        this.props.switchIsFetching({isFetching : true});
            CommentsService.postComment({data : {...commentData, articleId : this.props.article.id},
                                        alert : this.props.alert, user : this.props.user}).then(response => {
                if ((response.status === 200)) {
                    CommentsService.getComments({articleId : this.props.article.id,
                                                 alert : this.props.alert, user : this.props.user}).then(data => {this.props.setComments({comments : data})}); 
                }
           });
        this.props.switchIsFetching({isFetching : false});
   }

   postLike(likeData){
        this.props.switchIsFetching({isFetching : true});
            LikesService.postLike({data : likeData, alert : this.props.alert, user : this.props.user}).then(response => {
                if ((response.status === 200)) {
                     ArticlesService.getChapters({articleId : this.props.article.id,
                                                  alert : this.props.alert, user : this.props.user}).then(data => {this.props.setChapters({chapters : data})}); 
                }
            });
        this.props.switchIsFetching({isFetching : false});
    }

    postRate(rate) {
        let data = {data : {value : rate, articleDto : this.props.article}, id : this.props.match.params.id, alert : this.props.alert, user : this.props.user};
        this.props.switchIsFetching({isFetching : true}); 
            ArticlesService.postRate(data).then(response => {
                if(response.status === 200) {
                 this.getArticle();
                }
        })
        this.props.switchIsFetching({isFetching : false});
   }

    deleteChapter(){
        this.props.switchIsFetching({isFetching : true});
            ChapterService.deleteChapter({id : this.props.checked, alert : this.props.alert, user : this.props.user}).then(response => {
                if ((response.status === 200)) {
                     ArticlesService.getChapters({articleId : this.props.article.id,
                                                  alert : this.props.alert, user : this.props.user}).then(data => {this.props.setChapters({chapters : data})}); 
                }
            });
        this.props.switchIsFetching({isFetching : false});
    }

    doAction(action){
        if(action === DELETE) {
            this.deleteChapter();
        } else if (action === EDIT) {
            this.props.history.push(`/chapters/${this.props.checked}`);
        } else if (action === CREATE) {
            this.props.history.push(`/chapters`);
        }
    }

    render() {
        return( 
            <div> 
                {this.props.isFetching ? <Preloader/> : null}
                <ArticleOverview article = {this.props.article}
                                 isOwner = {this.isOwner()}
                                 chapters = {this.props.chapters}
                                 checked = {this.props.checked}
                                 comments = {this.props.comments}
                                 doAction = {this.doAction}
                                 checkChapter = {this.props.checkChapter}
                                 postComment = {this.postComment}
                                 postLike = {this.postLike}
                                 postRate = {this.postRate}
                                 user = {this.props.user}/>
            </div>      
        )
    }
}

let mapStateToProps = (state) => {
    return {
        isFetching : state.usersPage.isFetching,
        user: state.loginPage.user,
        article : state.articleOverviewPage.article,
        comments : state.articleOverviewPage.comments,
        chapters : state.articleOverviewPage.chapters,
        checked : state.articleOverviewPage.checked
    }
}

let WithAlertArticleOverviewContainer = withAlert()(ArticleOverviewContainer);

let WithRouteArticleOverviewContainer = withRouter(WithAlertArticleOverviewContainer);

export default connect(mapStateToProps, {
    switchIsFetching, setArticle, setComments, setChapters, checkChapter, 
    })(WithRouteArticleOverviewContainer);