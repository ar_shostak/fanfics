import React from 'react';
import {Field, reduxForm, reset} from 'redux-form';
import { Container, Row, Col } from 'react-bootstrap';
import { required } from './../../validator/CrUpdValidator';
import {TextArea} from './../../utils/CustomTags';
import translate from './../../i18n/translate';

let Comments = (props) => {

    let handleSubmit = (data) => {
        props.postComment({text : data.text});

    }

    let isUserAuthenticated = () => {
        return props.user.id === '' ? false : true;
    }

    let getForm = () => {
        if (isUserAuthenticated() === true) {
            return (<CommentReduxForm onSubmit = {handleSubmit} isUserAuthenticated = {isUserAuthenticated}/>);
        } else {
            return (<div/>);
        }
    }
   
    return (
        <Container>
            <Row>
                {props.comments.map(
                    (comment) => 
                        <Container fluid>
                            <Row >
                                <Col md = 'auto'>{comment.text} </Col>
                                <Col/>
                            </Row>
                            <Row>
                                <Col>{comment.user.firstName}</Col>
                                <Col>{comment.createdDate}</Col>
                            </Row>
                        </Container>
                )}
            </Row>
            <Row >
                <Col xs = {10}>{getForm()}</Col>
                <Col/>
            </Row>     
        </Container>
    ) 
}

const CommentForm = (props) => {
  
    return(
        <form onSubmit={props.handleSubmit}>
            <Field placeholder = {'comment-placeholder'} name = {'text'} component = {TextArea}
                        validate = {required} className = {'loginInput'} />
            <button type = 'submit'
                         disabled = {props.isUserAuthenticated() === true ? false : true}
                         className = {'loginButton'}>{translate('leave-comment')}</button>
        </form>
    )
}

const afterSubmit = (result, dispatch) =>
  dispatch(reset('commentForm'));

const CommentReduxForm = reduxForm({
    form: 'commentForm',
    onSubmitSuccess: afterSubmit,
})(CommentForm);

export default Comments;