import React from 'react';
import { Container, Row, Col } from 'react-bootstrap';
import { Rating } from './../../utils/CustomTags';
import translate from './../../i18n/translate';
import Chapters from './Chapters';
import Comments from './Comments';


let Content = (props) => {

    let postRate = (value) => {
        props.postRate(value);
    }

    let showRates = () => {
        if(props.isRateLeft === false && props.user.id !== '') {
            return ( 
                <Row>
                    <Row >
                        <Col><h5>{translate('leave-a-rate')}</h5></Col>
                        <Col></Col>
                    </Row>
                    <Row>
                        <Col><Rating postRate = {postRate} /></Col>
                    </Row>
                </Row>);
        }
    }

    return (
        <Container>
            <Row ><Chapters chapters = {props.chapters} user = {props.user} postLike = {props.postLike}/></Row>
            {showRates()}
            <Row ><Comments postComment = {props.postComment} comments = {props.comments} user = {props.user}/></Row>
        </Container>
    )
}

export default Content;