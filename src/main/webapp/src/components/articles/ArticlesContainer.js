import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import {connect} from 'react-redux';
import { withAlert } from 'react-alert'
import ArticlesService from './../service/ArticlesService';
import UsersService from './../service/UsersService';
import Preloader from '../preloader/Preloader';
import Articles from './Articles';
import { setUser } from './../store/LoginReducer';
import { switchIsFetching } from './../store/UsersReducer';
import { setArticles, checkArticle } from './../store/ArticlesReducer';

class ArticlesContainer extends Component {

    constructor(props){
        super(props);
        this.componentDidMount = this.componentDidMount.bind(this);
        this.checkArticle = this.checkArticle.bind(this);
        this.deleteArticle = this.deleteArticle.bind(this);
        this.searchText = this.searchText.bind(this);
        this.isOnMyPage = this.isOnMyPage.bind(this);
        this.getAllArticles = this.getAllArticles.bind(this);
        this.getArticles = this.getArticles.bind(this);
        this.getMyArticles = this.getMyArticles.bind(this);
    }

    isOnMyPage(){
        return this.props.match.params.myArticles !== undefined;
    }
    
    componentDidMount(){
       this.getArticles();
    }

    componentDidUpdate(prevState){
        if(this.props.match.params.myArticles !== prevState.match.params.myArticles) {
            this.getArticles();
        }
    }

    getArticles() {
        this.props.switchIsFetching({isFetching : true});
        if (this.isOnMyPage() === true) {
            this.getMyArticles();
        } else {
            this.getAllArticles();
        }
        this.props.switchIsFetching({isFetching : false});
    }

    getAllArticles(){
        ArticlesService.retrieveAllArticles({alert : this.props.alert}).then(data => {
            this.props.setArticles({articles : data});
        });
    }

    getMyArticles(){
        UsersService.retrieveMyArticles({alert : this.props.alert,
                                         user : this.props.user,
                                         id : this.props.user.id }).then(data => {
            this.props.setArticles({articles : data});
        });
    }

    deleteArticle(){
        this.props.switchIsFetching({isFetching : true});
        ArticlesService.deleteArticle({id : this.props.checked.id, alert : this.props.alert,
                                       user : this.props.user}).then(response => {
              if (response.status === 200) {
                  this.props.checkArticle({checked : []});
                  this.getMyArticles();
              }
        })
        this.props.switchIsFetching({isFetching : false});
    }

    checkArticle(article){
        this.props.checkArticle({checked : article});
    }

    searchText(data){
        this.props.switchIsFetching({isFetching : true});

        ArticlesService.searchArticles({data : data.text, alert : this.props.alert,
                                        user : this.props.user}).then(response => {     
                this.props.setArticles({articles : response});
                this.props.checkArticle({checked : []});
      })
      this.props.switchIsFetching({isFetching : false});

    }

    render() {
        return( 
            <div> 
                {this.props.isFetching ? <Preloader/> : null}
                <Articles articles = {this.props.articles}
                          isOnMyPage = {this.isOnMyPage()}
                          checked = {this.props.checked}
                          searchText = {this.searchText}
                          deleteArticle = {this.deleteArticle}
                          checkArticle = {this.checkArticle}
                          user = {this.props.user}/>
            </div>      
        )
    }
}


let mapStateToProps = (state) => {
    return {
        isFetching : state.usersPage.isFetching,
        user: state.loginPage.user,
        articles : state.articlesPage.articles,
        checked : state.articlesPage.checked,
    }
}

let WithAlertArticlesContainer = withAlert()(ArticlesContainer);

let WithRouteArticlesContainer = withRouter(WithAlertArticlesContainer);

export default connect(mapStateToProps, {
    setUser, switchIsFetching, checkArticle, setArticles
    })(WithRouteArticlesContainer);