import React from 'react';
import {Field, reduxForm, reset} from 'redux-form';
import {Row, Col } from 'react-bootstrap';
import {TextArea} from '../../../utils/CustomTags';
import search from './../../../../img/search.png'
import './Search.css';

let Search = (props) => {

    let handleSubmit = (data) => {
        props.searchText({text : data.text});

    }

    let isUserAuthenticated = () => {
        return props.user.id === '' ? false : true;
    }

    let getForm = () => {
        if (isUserAuthenticated() === true) {
            return (<SearchReduxForm onSubmit = {handleSubmit} isUserAuthenticated = {isUserAuthenticated}/>);
        } else {
            return (<div/>);
        }
    }
   
    return (
       <Col>{getForm()}</Col>
    ) 
}

const SearchForm = (props) => {
  
    return(
            <form onSubmit={props.handleSubmit}>
                <Row>
                   <Col xs = {8}><Field placeholder = {'search-placeholder'} name = {'text'} component = {TextArea}
                                         className = {'searchInput'} /></Col>
                   <button type = 'submit'
                                 disabled = {props.isUserAuthenticated() === true ? false : true}
                                 className = {'loginButton'}><img src = {search} width = '30' alt = ''/></button>
                </Row>
            </form>
    )
}

const afterSubmit = (result, dispatch) =>
  dispatch(reset('searchForm'));

const SearchReduxForm = reduxForm({
    form: 'searchForm',
    onSubmitSuccess: afterSubmit,
})(SearchForm);

export default Search;