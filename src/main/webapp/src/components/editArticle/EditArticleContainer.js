import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import {connect} from 'react-redux';
import { withAlert } from 'react-alert';
import { checkArticle, setEnums } from './../store/ArticlesReducer';
import { switchIsFetching } from './../store/UsersReducer';
import EditArticle from './EditArticle';
import ArticlesService from './../service/ArticlesService';
import Preloader from './../preloader/Preloader';
 
class EditArticleContainer extends Component {
    constructor(props){
        super(props);
        this.componentDidMount.bind(this);
        this.getArticle.bind(this);
        this.getEnums.bind(this);
        this.produceArticle = this.produceArticle.bind(this);
        this.state = {
            id : this.props.match.params.id
        }
    };

    componentDidMount(){
        this.getEnums();
        if(this.state.id !== undefined) {
            this.props.switchIsFetching({isFetching : true});
            this.getArticle();
            this.props.switchIsFetching({isFetching : false});
        }
    }

    getEnums() {
        ArticlesService.getEnums({user : this.props.user, alert : this.props.alert}).then(data => {
            this.props.setEnums({enumsMap : data});
        })
    }

    getArticle() {
        ArticlesService.getArticle({articleId : this.state.id,
                                    alert : this.props.alert,
                                    user : this.props.user}).then(data => {
            this.props.checkArticle({checked : data});
        })
    }

    componentWillUnmount() {
        this.props.checkArticle({checked : []});
    }

    produceArticle(newArticle) {
        let data = {data : newArticle, id : this.state.id, alert : this.props.alert, user : this.props.user};
        this.props.switchIsFetching({isFetching : true}); 
        if(this.state.id === undefined) {
            ArticlesService.postArticle(data).then(response => {
                if(response.status === 201) {
                    this.props.history.push(`/articles/${response.data.id}`)
                }
            })
        } else {
            ArticlesService.putArticle(data).then(response => {
                if(response.status === 200) {
                    this.props.history.push(`/articles/${response.data.id}`)
                }
           })
        }
        this.props.switchIsFetching({isFetching : false});
   }

    render() {
        return( 
            <div> 
                {this.props.isFetching ? <Preloader/> : null}
                <EditArticle user = {this.props.user}
                             enumsMap = {this.props.enumsMap}
                             article = {this.props.checked}
                             produceArticle = {this.produceArticle} />
            </div>      
        )
    }
}

let mapStateToProps = (state) => {
    return {
        user: state.loginPage.user,
        checked : state.articlesPage.checked,
        chapter : state.editChapterPage.chapter,
        enumsMap : state.articlesPage.enumsMap,
    }
}
let WithAlertEditArticleContainer = withAlert()(EditArticleContainer);

let WithRouteEditArticleContainer = withRouter(WithAlertEditArticleContainer);

export default connect(mapStateToProps, {
    switchIsFetching, checkArticle, setEnums
    })(WithRouteEditArticleContainer);