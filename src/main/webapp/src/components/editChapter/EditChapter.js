import React from 'react';
import { Field, reduxForm } from 'redux-form';
import { TextArea, WrapedTextArea, SingleFileDropZone } from './../utils/CustomTags';
import translate from '../i18n/translate';
import { required } from './../validator/CrUpdValidator';
import {connect} from 'react-redux';

let EditChapter = (props) => {
    let article = props.article;
    const handleSubmit = (formData) => {
        const data = new FormData();

        for(let key in formData) {
            if(formData[key] !== undefined) {
                if(key === 'image') {
                    data.append(`file`, formData[key], `${formData[key].name}`);
                }
                data.append(key, formData[key])
            }
        }
        data.append('articleId', article.id);
        props.produceChapter(data); 
    }

    let fromBase64 = (b64Data, contentType, sliceSize) => {
        contentType = contentType || '';
        sliceSize = sliceSize || 512;

        var byteCharacters = atob(b64Data);
        var byteArrays = [];

        for (var offset = 0; offset < byteCharacters.length; offset += sliceSize) {
            var slice = byteCharacters.slice(offset, offset + sliceSize);

            var byteNumbers = new Array(slice.length);
            for (var i = 0; i < slice.length; i++) {
                byteNumbers[i] = slice.charCodeAt(i);
            }

            var byteArray = new Uint8Array(byteNumbers);

            byteArrays.push(byteArray);
        }

      var blob = new Blob(byteArrays, {type: contentType});
      return blob;
}

    let getExistingFile = () => {
        let chapterWithDecodedFile = props.chapter;
        if( props.chapter.attachment !== null) {
            let existingFile = props.chapter.attachment;
            let decoded = fromBase64(existingFile.blobBytes, existingFile.contentType, existingFile.blobBytes.length);
            chapterWithDecodedFile.attachment = new File([decoded],
                `${existingFile.name}`,
                {type : existingFile.contentType, lastModified:new Date()})
        }
        return chapterWithDecodedFile
    }

    let getPlaceholders = () => {
        
        if (props.chapter.id === undefined) {
            return {name : 'name',
            number : 'number',
            content : 'content'};
        } else {
            return getExistingFile();
        }
    }                    

    return(
        <div>
          <div>{translate('fill-in-headline')}</div>  
          <EditChapterReduxForm onSubmit = {handleSubmit}
                                chapter = {props.chapter}
                                placeholders = {getPlaceholders()}/>
        </div>
    )
}

const EditChapterForm = (props) => {
    return(
        <div className = 'formContainer'>
            <form onSubmit={props.handleSubmit}> 
                <Field placeholder = {`${props.placeholders.name}`} name = 'name' validate = {required}
                            component = {TextArea} object = {props.chapter.name} className='ticketInput'/>  
                <Field placeholder = {`${props.placeholders.number}`} name = 'number' validate = {required}
                            component = {TextArea} object = {props.chapter.number} className='ticketInput'/>
                <Field placeholder = {`${props.placeholders.content}`} defaultValue = {props.chapter.content} name = {'content'}
                            component = {WrapedTextArea} object = {props.chapter.content} className='ticketInput'/> 
                <Field name = {'image'} placeholder = {props.placeholders.attachment} component = {SingleFileDropZone} type = {'file'} label = {'Browse'}  className = {'file'}/>
                <div><button type = 'submit' className = 'button'>{translate('save')}</button></div>
            </form>
        </div>
    )
}

const mapStateToProps = (state, props) => ({
    initialValues:  {content:state.editChapterPage.chapter.content}
  })
  
const EditChapterReduxForm = connect(mapStateToProps)(reduxForm({form: 'editChapterForm',  enableReinitialize: true })(EditChapterForm))

export default EditChapter;