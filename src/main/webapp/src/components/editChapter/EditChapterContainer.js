import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import {connect} from 'react-redux';
import { withAlert } from 'react-alert';
import { setNewChapter } from './../store/EditChapterReducer';
import { switchIsFetching } from './../store/UsersReducer';
import EditChapter from './EditChapter';
import ChapterService from './../service/ChapterService';
import Preloader from './../preloader/Preloader';
 
class EditChapterContainer extends Component {
    constructor(props){
        super(props);
        this.componentDidMount.bind(this);
        this.produceChapter = this.produceChapter.bind(this);
    }

    componentDidMount(){
        if(this.props.match.params.id !== undefined) {
            this.props.switchIsFetching({isFetching : true});
            ChapterService.getChapter({chapterId : this.props.match.params.id,
                                       alert : this.props.alert,
                                       user : this.props.user}).then(data => {
                this.props.setNewChapter({chapter : data});
            })
            this.props.switchIsFetching({isFetching : false});
        }
    }

    componentWillUnmount() {
        this.props.setNewChapter({chapter : {}});
    }

    produceChapter(newChapter) {
        let data = {data : newChapter, id : this.props.match.params.id, alert : this.props.alert, user : this.props.user};
        this.props.switchIsFetching({isFetching : true}); 
        if(this.props.chapter.id === undefined) {
            ChapterService.postChapter(data).then(response => {
                if(response.status === 201) {
                    this.props.history.push(`/articles/${this.props.article.id}`)
                }
            })
        } else {
            ChapterService.putChapter(data).then(response => {
                if(response.status === 200) {
                    this.props.history.push(`/articles/${this.props.article.id}`)
                }
           })
        }
        this.props.switchIsFetching({isFetching : false});
   }

    render() {
        return( 
            <div> 
                {this.props.isFetching ? <Preloader/> : null}
                <EditChapter user = {this.props.user}
                             article = {this.props.article}
                             produceChapter = {this.produceChapter}
                             chapter = {this.props.chapter}/>
            </div>      
        )
    }
}

let mapStateToProps = (state) => {
    return {
        user: state.loginPage.user,
        article : state.articleOverviewPage.article,
        chapter : state.editChapterPage.chapter,
    }
}
let WithAlertEditChapterContainer = withAlert()(EditChapterContainer);

let WithRouteEditChapterContainer = withRouter(WithAlertEditChapterContainer);

export default connect(mapStateToProps, {
    switchIsFetching, setNewChapter
    })(WithRouteEditChapterContainer);