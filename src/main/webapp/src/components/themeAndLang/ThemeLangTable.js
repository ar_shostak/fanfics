import React from 'react';
import './ThemeLangTable.css';
import translate from './../i18n/translate';

const ThemeLangTable = (props) => {

    let switchLang = () => {
       props.setLanguage();
    }

    let switchTheme = () => {
        props.setTheme();
    }

    return (    
        <div>
             <nav>
                 <div className = 'line'>
                      <button onClick = {switchLang} disabled = {props.language === 'EN' ? true : false} className ='theam-lang-table-button'>EN</button>
                      <button onClick = {switchLang} disabled = {props.language === 'EN' ? false : true} className ='theam-lang-table-button'>RUS</button>
                 </div>
                 <div className = 'line'>
    <button onClick = {switchTheme} disabled = {props.theme === 'DARK' ? true : false} className ='theam-lang-table-button'>{translate('dark')}</button>
                      <button onClick = {switchTheme} disabled = {props.theme === 'DARK' ? false : true} className ='theam-lang-table-button'>{translate('light')}</button>
                 </div>
             </nav>
          </div>
    )
}

export default ThemeLangTable;