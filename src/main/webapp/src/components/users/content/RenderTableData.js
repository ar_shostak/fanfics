import React from 'react';
import { Checkbox } from '../../utils/CustomTags';
import translate from '../../i18n/translate';

let RenderTableData = (props) => {

    let handleChange =(e) => {
       props.checkUser(e);
    }

        let checkAll =() => {
        props.checkAll();
        }

    let header = () => {
        return (
            <thead>
                <tr>
                    <th class='col-1'><Checkbox checked={props.flag} onChange={checkAll} />
                    {translate('select-deselect-column')}</th>
                    <th class='col-2'>{translate('ID-column')}</th>
                    <th class='col-3'>{translate('name-column')}</th>
                    <th >{translate('email-column')}</th>
                    <th class='col-3'>{translate('registration-date-column')}</th>
                    <th class='col-3'>{translate('last-login-date-column')}</th>
                    <th class='col-3'>{translate('status-column')}</th>
                </tr>
            </thead>
        )
    };

    let body = (props) => {
        return (
            props.users.map(
                user =>
                    <tbody>
                        <tr key={user.id}>
                            <td><Checkbox name={user.id}
                                          checked={props.checkedItems.get(user.id)} onChange={handleChange} /></td>
                            <td>{user.id}</td>
                            <td>{user.firstName}</td>
                            <td>{user.email}</td>
                            <td>{user.registrationDate}</td>
                            <td>{user.loginDate}</td>
                            <td>{user.state}</td>
                        </tr>
                    </tbody>
            )
        )
    }

    return (
         <table class="table">
            {header()}
            {body(props)}
        </table>
    )
}

export default RenderTableData;